<?php

namespace byiitrix\console\controllers;

use yii\helpers\FileHelper;
use yii\console\Controller;

/**
 * PhpDoc generator for shortcut helpers from database
 *
 * @package byiitrix\console\controllers
 */
class HelperController extends Controller
{
    private function helperNamespace()
    {
        return 'common\components';
    }

    private function helperPattern()
    {
        return '#( \* @(?:begin|internal\s*\{begin\})).*( \* @(?:end|internal\s\{end\}))#Us';
    }

    private function helperPath()
    {
        $namespace = $this->helperNamespace();
        $path      = APP_ROOT . '/' . str_replace('\\', '/', $namespace);

        if (is_dir($path) === false) {
            FileHelper::createDirectory($path, defined('BX_DIR_PERMISSIONS') ? BX_DIR_PERMISSIONS : 02775, true);
        }

        return $path;
    }

    /**
     * Generate phpdoc properties for all available helpers
     */
    public function actionAll()
    {
        $this->actionBlock();
        $this->actionElement();
        $this->actionProperty();
        $this->actionPropertyEnum();
        $this->actionSection();
    }

    /**
     * Properties for \byiitrix\Core::$block helper
     */
    public function actionBlock()
    {
        $namespace = $this->helperNamespace();
        $path      = $this->helperPath();
        $className = 'Block';
        $fileName  = "{$path}/{$className}.php";

        $idComments  = '';
        $getComments = '';

        $sql = <<<SQL
SELECT `b`.`CODE`, `b`.`IBLOCK_TYPE_ID`
FROM `b_iblock` AS `b`
INNER JOIN `b_iblock_type` AS `bt` ON `bt`.`ID` = `b`.`IBLOCK_TYPE_ID`
WHERE `b`.`CODE` != ''
ORDER BY `bt`.`SORT`, `bt`.`ID`, `b`.`SORT`, `b`.`ID`;
SQL;

        $command = \Yii::$app->getDb()->createCommand($sql);

        foreach ($command->queryAll() as $row) {
            $code = $row['CODE'];
            $type = $row['IBLOCK_TYPE_ID'];

            if (preg_match('#[/\#\-\!\?\=]+#', $code . $type)) {
                continue;
            }

            $idComments  .= "\n * @property int   \$id__{$code}__from__{$type}";
            $getComments .= "\n * @property array \$get__{$code}__from__{$type}";
        }

        $idComments  = trim($idComments);
        $getComments = trim($getComments);

        $internalBody = <<<BODY
 {$idComments}
 *
 {$getComments}
BODY;

        $contents = <<<PHP
<?php

namespace {$namespace};

/**
 * @begin
{$internalBody}
 * @end
 */
class {$className} extends \byiitrix\components\\{$className}
{
}

PHP;

        if (file_exists($fileName)) {
            $pattern          = $this->helperPattern();
            $originalContents = file_get_contents($fileName);

            if (preg_match($pattern, $originalContents)) {
                $contents = preg_replace($pattern, " * @begin\n{$internalBody}\n * @end", $originalContents);
            }
        }

        file_put_contents($fileName, $contents);
    }

    /**
     * Properties for \byiitrix\Core::$block helper
     */
    public function actionElement()
    {
        $namespace = $this->helperNamespace();
        $path      = $this->helperPath();
        $className = 'Element';
        $fileName  = "{$path}/{$className}.php";

        $listOfComments = '';

        $sql = <<<SQL
SELECT 
    `b_iblock`.`CODE`, 
    `b_iblock`.`IBLOCK_TYPE_ID`

FROM `b_iblock`

INNER JOIN `b_iblock_type` 
    ON `b_iblock_type`.`ID` = `b_iblock`.`IBLOCK_TYPE_ID`

WHERE `b_iblock`.`CODE` != ''

ORDER BY `b_iblock_type`.`SORT`, `b_iblock_type`.`ID`, `b_iblock`.`SORT`, `b_iblock`.`ID`;
SQL;

        $command = \Yii::$app->getDb()->createCommand($sql);

        foreach ($command->queryAll(\PDO::FETCH_NUM) as list($code, $type)) {
            if (preg_match('#[/\#\-\!\?\=]+#', $code . $type)) {
                continue;
            }

            $listOfComments .= "\n * @property array \$list_of__{$code}__from__{$type}";
        }

        $listOfComments = trim($listOfComments);

        $internalBody = <<<BODY
 {$listOfComments}
BODY;

        $contents = <<<PHP
<?php

namespace {$namespace};

/**
 * @begin
{$internalBody}
 * @end
 */
class {$className} extends \byiitrix\components\\{$className}
{
}

PHP;

        if (file_exists($fileName)) {
            $pattern          = $this->helperPattern();
            $originalContents = file_get_contents($fileName);

            if (preg_match($pattern, $originalContents)) {
                $contents = preg_replace($pattern, " * @begin\n{$internalBody}\n * @end", $originalContents);
            }
        }

        file_put_contents($fileName, $contents);
    }

    /**
     * Properties for \byiitrix\Core::$property helper
     */
    public function actionProperty()
    {
        $namespace = $this->helperNamespace();
        $path      = $this->helperPath();
        $className = 'Property';
        $fileName  = "{$path}/{$className}.php";

        $idComments  = '';
        $getComments = '';

        $sql = <<<SQL
SELECT 
    `bp`.`CODE` AS `PROPERTY_CODE`,
    `bp`.`PROPERTY_TYPE` AS `PROPERTY_TYPE`,
    `b`.`CODE` AS `IBLOCK_CODE`,
    `b`.`IBLOCK_TYPE_ID`
FROM `b_iblock_property` AS `bp`
INNER JOIN `b_iblock` AS `b` ON `b`.`ID` = `bp`.`IBLOCK_ID`
INNER JOIN `b_iblock_type` AS `bt` ON `bt`.`ID` = `b`.`IBLOCK_TYPE_ID`
WHERE `b`.`CODE` != ''
ORDER BY `bt`.`SORT`, `bt`.`ID`, `b`.`SORT`, `b`.`ID`, `bp`.`SORT`, `bp`.`ID`;
SQL;

        $command = \Yii::$app->getDb()->createCommand($sql);

        foreach ($command->queryAll() as $row) {
            $property = $row['PROPERTY_CODE'];
            $block    = $row['IBLOCK_CODE'];
            $type     = $row['IBLOCK_TYPE_ID'];

            if (preg_match('#[/\#\-\!\?\=]+#', $property . $block . $type)) {
                continue;
            }

            $idComments  .= "\n * @property int   \$id__{$property}__in__{$block}__from__{$type}";
            $getComments .= "\n * @property array \$get__{$property}__in__{$block}__from__{$type}";
        }

        $idComments  = trim($idComments);
        $getComments = trim($getComments);

        $internalBody = <<<BODY
 {$idComments}
 *
 {$getComments}
BODY;

        $contents = <<<PHP
<?php

namespace {$namespace};

/**
 * @begin
{$internalBody}
 * @end
 */
class {$className} extends \byiitrix\components\\{$className}
{
}

PHP;

        if (file_exists($fileName)) {
            $pattern          = $this->helperPattern();
            $originalContents = file_get_contents($fileName);

            if (preg_match($pattern, $originalContents)) {
                $contents = preg_replace($pattern, " * @begin\n{$internalBody}\n * @end", $originalContents);
            }
        }

        file_put_contents($fileName, $contents);
    }

    /**
     * Properties for \byiitrix\Core::$propertyEnum helper
     */
    public function actionPropertyEnum()
    {
        $namespace = $this->helperNamespace();
        $path      = $this->helperPath();
        $className = 'PropertyEnum';
        $fileName  = "{$path}/{$className}.php";

        $idComments     = '';
        $getComments    = '';
        $listOfComments = '';

        $sql = <<<SQL
SELECT 
    `bpe`.`XML_ID`,
    `bp`.`CODE` AS `PROPERTY_CODE`,
    `bp`.`PROPERTY_TYPE` AS `PROPERTY_TYPE`,
    `b`.`CODE` AS `IBLOCK_CODE`,
    `b`.`IBLOCK_TYPE_ID`
FROM `b_iblock_property_enum` AS `bpe`
INNER JOIN `b_iblock_property` AS `bp` ON `bp`.`ID` = `bpe`.`PROPERTY_ID`
INNER JOIN `b_iblock` AS `b` ON `b`.`ID` = `bp`.`IBLOCK_ID`
INNER JOIN `b_iblock_type` AS `bt` ON `bt`.`ID` = `b`.`IBLOCK_TYPE_ID`
WHERE `b`.`CODE` != ''
ORDER BY `bt`.`SORT`, `bt`.`ID`, `b`.`SORT`, `b`.`ID`, `bp`.`SORT`, `bp`.`ID`, `bpe`.`SORT`, `bpe`.`ID`;
SQL;

        $command = \Yii::$app->getDb()->createCommand($sql);

        foreach ($command->queryAll() as $row) {
            $xmlID    = $row['XML_ID'];
            $property = $row['PROPERTY_CODE'];
            $block    = $row['IBLOCK_CODE'];
            $type     = $row['IBLOCK_TYPE_ID'];

            if (preg_match('#[/\#\-\!\?\=]+#', $xmlID . $property . $block . $type)) {
                continue;
            }

            $idComments  .= "\n * @property int   \$id__{$xmlID}__of__{$property}__in__{$block}__from__{$type}";
            $getComments .= "\n * @property array \$get__{$xmlID}__of__{$property}__in__{$block}__from__{$type}";
        }

        $sql = <<<SQL
SELECT 
    `bp`.`CODE` AS `PROPERTY_CODE`,
    `bp`.`PROPERTY_TYPE` AS `PROPERTY_TYPE`,
    `b`.`CODE` AS `IBLOCK_CODE`,
    `b`.`IBLOCK_TYPE_ID`
FROM `b_iblock_property` AS `bp`
INNER JOIN `b_iblock` AS `b` ON `b`.`ID` = `bp`.`IBLOCK_ID`
INNER JOIN `b_iblock_type` AS `bt` ON `bt`.`ID` = `b`.`IBLOCK_TYPE_ID`
WHERE `bp`.`PROPERTY_TYPE` = 'L' AND `b`.`CODE` != ''
ORDER BY `bt`.`SORT`, `bt`.`ID`, `b`.`SORT`, `b`.`ID`, `bp`.`SORT`, `bp`.`ID`;
SQL;

        $command = \Yii::$app->getDb()->createCommand($sql);

        foreach ($command->queryAll() as $row) {
            $property = $row['PROPERTY_CODE'];
            $block    = $row['IBLOCK_CODE'];
            $type     = $row['IBLOCK_TYPE_ID'];

            if (preg_match('#[/\#\-\!\?\=]+#', $property . $block . $type)) {
                continue;
            }

            $listOfComments .= "\n * @property array \$list_of__{$property}__in__{$block}__from__{$type}";
        }

        $listOfComments = trim($listOfComments);
        $idComments     = trim($idComments);
        $getComments    = trim($getComments);

        $internalBody = <<<BODY
 {$listOfComments}
 *
 {$idComments}
 *
 {$getComments}
BODY;

        $contents = <<<PHP
<?php

namespace {$namespace};

/**
 * @begin
{$internalBody}
 * @end
 */
class {$className} extends \byiitrix\components\\{$className}
{
}

PHP;

        if (file_exists($fileName)) {
            $pattern          = $this->helperPattern();
            $originalContents = file_get_contents($fileName);

            if (preg_match($pattern, $originalContents)) {
                $contents = preg_replace($pattern, " * @begin\n{$internalBody}\n * @end", $originalContents);
            }
        }

        file_put_contents($fileName, $contents);
    }

    /**
     * Properties for \byiitrix\Core::$section helper
     */
    public function actionSection()
    {
        $namespace = $this->helperNamespace();
        $path      = $this->helperPath();
        $className = 'Section';
        $fileName  = "{$path}/{$className}.php";

        $listOfComments     = '';
        $rootListOfComments = '';

        $sql = <<<SQL
SELECT 
    `b_iblock`.`CODE`, 
    `b_iblock`.`IBLOCK_TYPE_ID`

FROM `b_iblock`

INNER JOIN `b_iblock_type` 
    ON `b_iblock_type`.`ID` = `b_iblock`.`IBLOCK_TYPE_ID`

WHERE `b_iblock`.`CODE` != ''

ORDER BY 
    `b_iblock_type`.`SORT`, 
    `b_iblock_type`.`ID`, 
    `b_iblock`.`SORT`, 
    `b_iblock`.`ID`;
SQL;

        $command = \Yii::$app->getDb()->createCommand($sql);

        foreach ($command->queryAll(\PDO::FETCH_NUM) as list($code, $type)) {
            if (preg_match('#[/\#\-\!\?\=]+#', $code . $type)) {
                continue;
            }

            $listOfComments     .= "\n * @property array \$list_of__{$code}__from__{$type}";
            $rootListOfComments .= "\n * @property array \$root_list_of__{$code}__from__{$type}";
        }

        $listOfComments     = trim($listOfComments);
        $rootListOfComments = trim($rootListOfComments);

        $internalBody = <<<BODY
 {$listOfComments}
 *
 {$rootListOfComments}
BODY;

        $contents = <<<PHP
<?php

namespace {$namespace};

/**
 * @begin
{$internalBody}
 * @end
 */
class {$className} extends \byiitrix\components\\{$className}
{
}

PHP;

        if (file_exists($fileName)) {
            $pattern          = $this->helperPattern();
            $originalContents = file_get_contents($fileName);

            if (preg_match($pattern, $originalContents)) {
                $contents = preg_replace($pattern, " * @begin\n{$internalBody}\n * @end", $originalContents);
            }
        }

        file_put_contents($fileName, $contents);
    }
}
