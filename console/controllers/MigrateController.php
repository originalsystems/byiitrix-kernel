<?php

namespace byiitrix\console\controllers;

use byiitrix\helpers\Utils;

class MigrateController extends \yii\console\controllers\MigrateController
{
    public $templateFile  = '@byiitrix/views/migration.php';
    public $migrationPath = '@console/migrations';

    public $eventTemplateFile      = '@byiitrix/views/migration-event.php';
    public $iblockTypeTemplateFile = '@byiitrix/views/migration-iblock-type.php';
    public $iblockTemplateFile     = '@byiitrix/views/migration-iblock.php';

    public function beforeAction($action)
    {
        Utils::flushCache();
        \Yii::$app->getCache()->flush();

        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        Utils::flushCache();
        \Yii::$app->getCache()->flush();

        return parent::afterAction($action, $result);
    }

    protected function generateMigrationSourceCode($params)
    {
        $name = $params['name'];

        if (preg_match('#^add_(?P<type>.+)_iblock_type$#', $name, $matches)) {
            return $this->renderFile(\Yii::getAlias($this->iblockTypeTemplateFile), array_merge($params, [
                'type' => mb_strtoupper($matches['type']),
            ]));
        }

        if (preg_match('#^add_(?P<code>.+)_from_(?P<type>.+)_iblock$#', $name, $matches)) {
            return $this->renderFile(\Yii::getAlias($this->iblockTemplateFile), array_merge($params, [
                'code' => mb_strtoupper($matches['code']),
                'type' => mb_strtoupper($matches['type']),
            ]));
        }

        if (preg_match('#^add_(?P<event_name>.+)_event$#', $name, $matches)) {
            return $this->renderFile(\Yii::getAlias($this->eventTemplateFile), array_merge($params, [
                'eventName' => mb_strtoupper($matches['event_name']),
            ]));
        }

        return parent::generateMigrationSourceCode($params);
    }
}
