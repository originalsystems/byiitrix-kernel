<?php

namespace byiitrix\console\controllers;

use yii\base\InvalidArgumentException;
use yii\console\Controller;
use yii\helpers\FileHelper;

class GenerateController extends Controller
{
    public function actionBitrixComponent($namespace, $component)
    {
        $pattern = '#^[a-z0-9\._-]+$#';

        if (preg_match($pattern, $namespace) === 0) {
            throw new InvalidArgumentException('Invalid namespace name');
        }

        if (preg_match($pattern, $component) === 0) {
            throw new InvalidArgumentException('Invalid component name');
        }

        $source = realpath(\Yii::getAlias('@byiitrix/views/bitrix-component'));
        $path   = \Yii::getAlias("@local/components/{$namespace}/{$component}");

        FileHelper::copyDirectory($source, $path, [
            'dirMode'   => defined('BX_DIR_PERMISSIONS') ? BX_DIR_PERMISSIONS : 02775,
            'afterCopy' => function ($from, $to) use ($namespace, $component) {
                if (is_file($to)) {
                    $cmd = <<<BASH
sed -i \
    -e 's/#NAMESPACE#/{$namespace}/g' \
    -e 's/#COMPONENT#/{$component}/g' \
    {$to}
BASH;

                    exec($cmd);
                }
            },
        ]);
    }
}
