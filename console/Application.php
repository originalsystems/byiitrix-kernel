<?php

namespace byiitrix\console;

use yii\console\controllers\AssetController;
use yii\console\controllers\CacheController;
use yii\console\controllers\FixtureController;
use yii\console\controllers\MessageController;
use yii\console\controllers\ServeController;
use byiitrix\console\controllers\GenerateController;
use byiitrix\console\controllers\HelpController;
use byiitrix\console\controllers\HelperController;
use byiitrix\console\controllers\MigrateController;

class Application extends \yii\console\Application
{
    public function coreCommands()
    {
        return [
            'asset'    => AssetController::class,
            'cache'    => CacheController::class,
            'generate' => GenerateController::class,
            'fixture'  => FixtureController::class,
            'help'     => HelpController::class,
            'helper'   => HelperController::class,
            'message'  => MessageController::class,
            'migrate'  => MigrateController::class,
            'serve'    => ServeController::class,
        ];
    }

    public function run()
    {
        $this->state = self::STATE_BEFORE_REQUEST;
        $this->trigger(self::EVENT_BEFORE_REQUEST);

        $this->state = self::STATE_HANDLING_REQUEST;
        $this->handleRequest($this->getRequest());

        \AddEventHandler('main', 'onAfterEpilog', [$this, 'flushLog']);
        \AddEventHandler('main', 'OnBeforeLocalRedirect', [$this, 'flushLog']);

        return 0;
    }

    public function flushLog()
    {
        $this->state = self::STATE_AFTER_REQUEST;
        $this->trigger(self::EVENT_AFTER_REQUEST);

        $this->state = self::STATE_SENDING_RESPONSE;
        $this->getResponse()->send();

        $this->state = self::STATE_END;

        \Yii::$app->getLog()->getLogger()->flush(true);
    }
}
