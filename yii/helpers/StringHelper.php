<?php

namespace yii\helpers;

class StringHelper extends BaseStringHelper
{
    public static function translit($input)
    {
        static $replace;

        if ($replace === null) {
            $replace = require __DIR__ . '/transliterate.php';
        }

        $input = \strtr($input, $replace);
        $input = \preg_replace('#\W#', '-', $input);
        $input = \preg_replace('#\-\-+#', '-', $input);
        $input = \iconv('UTF-8', 'ASCII//TRANSLIT', $input);

        return \strtolower(\trim($input, '-'));
    }

    /**
     * @param int    $n
     * @param string $none
     * @param string $single
     * @param string $one
     * @param string $few
     * @param string $many
     * @param string $other
     *
     * @return string mixed
     */
    public static function pluralize($n, $none, $single, $one, $few, $many, $other = null)
    {
        $other = $other !== null ? $other : $many;

        $pattern = "{n, plural, =0{{$none}} =1{{$single}} one{{$one}} few{{$few}} many{{$many}} other{{$other}}}";

        return \MessageFormatter::formatMessage('ru_RU', $pattern, ['n' => $n]);
    }

    /**
     * @param array $items
     * @param int   $level
     *
     * @return string
     */
    public static function recursiveMenu(array $items, $level = 0)
    {
        if (count($items) === 0) {
            return '';
        }

        $html = '<ul>';

        foreach ($items as $item) {
            $classList = [];

            if ($item['SELECTED']) {
                $classList[] = 'active';
            }

            $html .= '<li class="' . implode(' ', $classList) . '">';
            $html .= "<a href=\"{$item['LINK']}\" title=\"{$item['TEXT']}\">{$item['TEXT']}</a>";
            $html .= self::recursiveMenu($item['CHILDREN'], $level + 1);
            $html .= '</li>';
        }

        $html .= '</ul>';

        return $html;
    }

    public static function menuTree(array $items)
    {
        if (count($items) === 0) {
            return [];
        }

        $menu   = [];
        $first  = current($items);
        $level  = (int)$first['DEPTH_LEVEL'];
        $levels = [
            $level => &$menu,
        ];

        foreach ($items as $item) {
            $current = (int)$item['DEPTH_LEVEL'];

            $item['CHILDREN'] = [];

            if ($current > $level) {
                $levels[$current] = &$levels[$level][count($levels[$level]) - 1]['CHILDREN'];
            }

            $levels[$current][] = $item;

            $level = $current;
        }

        unset($level, $levels, $current);

        return $menu;
    }

    /**
     * Render simple menu from bitrix-formed array of menu items by component bitrix:menu
     *
     * @param array $items
     *
     * @return string
     */
    public static function menu(array $items)
    {
        if (count($items) === 0) {
            return '';
        }

        $menu = self::menuTree($items);

        return self::recursiveMenu($menu);
    }
}
