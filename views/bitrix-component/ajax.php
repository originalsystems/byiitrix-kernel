<?php

/**
 * @var CMain            $APPLICATION
 * @var CUser            $USER
 * @var CDatabase        $DB
 * @var CBitrixComponent $this
 * @var array            $arParams
 * @var array            $arResult
 * @var string           $componentName
 * @var string           $componentPath
 * @var string           $componentTemplate
 * @var string           $parentComponentName
 * @var string           $parentComponentPath
 * @var string           $parentComponentTemplate
 */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

header('Content-Type: application/json; charset=UTF-8');

$data   = [];
$errors = [];

switch (isset($_POST['action']) ? $_POST['action'] : null) {
    case 'REQUEST':
        break;

    default:
        $errors[] = 'Unknown action';
}

echo json_encode([
    'success' => count($errors) === 0,
    'errors'  => $errors,
    'data'    => $data,
]);
