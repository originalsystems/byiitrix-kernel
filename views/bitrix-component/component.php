<?php

/**
 * @var CMain            $APPLICATION
 * @var CUser            $USER
 * @var CDatabase        $DB
 * @var CBitrixComponent $this
 * @var array            $arParams
 * @var array            $arResult
 * @var string           $componentName
 * @var string           $componentPath
 * @var string           $componentTemplate
 * @var string           $parentComponentName
 * @var string           $parentComponentPath
 * @var string           $parentComponentTemplate
 */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (isset($_POST['component']) && $_POST['component'] === $this->getName()) {
    if ($_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') {
        $APPLICATION->RestartBuffer();

        require __DIR__ . '/ajax.php';

        exit;
    }
}

$this->includeComponentTemplate();
