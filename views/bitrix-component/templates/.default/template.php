<?php

/**
 * @var CMain                    $APPLICATION
 * @var CUser                    $USER
 * @var CDatabase                $DB
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent         $component
 * @var array                    $arParams
 * @var array                    $arResult
 * @var string                   $templateName
 * @var string                   $templateFile
 * @var string                   $templateFolder
 * @var string                   $componentPath
 */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<section class="section-#COMPONENT#" data-component="<?= $component->getName(); ?>">
    <form method="post">
        <input type="hidden" name="COMPONENT" value="<?= $component->getName(); ?>">
    </form>
</section>
