<?php

/**
 * @var CMain                    $APPLICATION
 * @var CUser                    $USER
 * @var CDatabase                $DB
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent         $component
 * @var array                    $arParams
 * @var array                    $arResult
 * @var string                   $templateName
 * @var string                   $templateFile
 * @var string                   $templateFolder
 * @var string                   $componentPath
 */

