<?php

/**
 * @var \yii\base\View $this
 * @var string         $eventName
 * @var string         $className
 */

echo "<?php\n";

if( !empty($namespace) ) {
    echo PHP_EOL . "namespace {$namespace};" . PHP_EOL;
}
?>

use yii\db\Migration;

class <?= $className; ?> extends Migration
{
    public function safeUp()
    {
        $description = <<<TXT
#USERNAME# - Имя пользователя
#PAGE_URL# - Адрес страницы
#TIME# - Время отправки
#ADMIN_URL# - Ссылка в админке
TXT;
        \CEventType::Add([
            'LID'         => 'ru',
            'EVENT_NAME'  => '<?= $eventName; ?>',
            'NAME'        => 'Новое событие',
            'DESCRIPTION' => $description,
        ]);

        \CEventType::Add([
            'LID'         => 'en',
            'EVENT_NAME'  => '<?= $eventName; ?>',
            'NAME'        => 'Новое событие',
            'DESCRIPTION' => $description,
        ]);

        $message = new \CEventMessage();
        $text    = <<<TXT
#SITE_NAME# - Новое событие

Имя пользователя: #USERNAME#
Адрес страницы: #PAGE_URL#
Время отправки: #TIME#

Ссылка в админке: #ADMIN_URL#
TXT;

        $message->Add([
            'ACTIVE'     => 'Y',
            'EVENT_NAME' => '<?= $eventName; ?>',
            'LID'        => \core\Codex::SITE_ID_MAIN,
            'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
            'EMAIL_TO'   => '#DEFAULT_EMAIL_FROM#',
            'SUBJECT'    => '#SITE_NAME#: Новое событие',
            'BODY_TYPE'  => 'text',
            'MESSAGE'    => $text,
        ]);
    }

    public function safeDown()
    {
        $by     = $order = [];
        $result = \CEventMessage::GetList($by, $order, [
            'EVENT_NAME' => '<?= $eventName; ?>',
        ]);

        while ($row = $result->GetNext()) {
            \CEventMessage::Delete($row['ID']);
        }
        CEventType::Delete([
            'EVENT_NAME' => '<?= $eventName; ?>',
        ]);
    }
}
