<?php

/**
 * @var string $className the new migration class name without namespace
 * @var string $namespace the new migration class namespace
 */

echo "<?php\n";

if( !empty($namespace) ) {
    echo PHP_EOL . "namespace {$namespace};" . PHP_EOL;
}
?>

use yii\db\Migration;

class <?= $className; ?> extends Migration
{
    public function safeUp()
    {
        return true;
    }

    public function safeDown()
    {
        return true;
    }
}
