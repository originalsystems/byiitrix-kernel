<?php

/**
 * @var \yii\base\View $this
 * @var string         $code
 * @var string         $type
 */

echo "<?php\n";

if( !empty($namespace) ) {
    echo PHP_EOL . "namespace {$namespace};" . PHP_EOL;
}
?>

use yii\db\Migration;

class <?= $className; ?> extends Migration
{
    public function safeUp()
    {
        $block   = new \CIBlock();
        $blockID = $block->Add([
            'ACTIVE'           => 'Y',
            'CODE'             => \core\Codex::BLOCK_<?= $code; ?>,
            'SITE_ID'          => \core\Codex::SITE_ID_MAIN,
            'IBLOCK_TYPE_ID'   => \core\Codex::TYPE_<?= $type; ?>,
            'NAME'             => '<?= \yii\helpers\Inflector::humanize(mb_strtolower($code)); ?>',
            'PICTURE'          => null,
            'DESCRIPTION'      => '',
            'DESCRIPTION_TYPE' => null,
            'SORT'             => 100,
            //'LIST_MODE'        => 'S', // Разделы и элементы отдельно
            'LIST_MODE'        => 'C', // Разделы и элементы совместно
            'LIST_PAGE_URL'    => '',
            'SECTION_PAGE_URL' => '',
            'DETAIL_PAGE_URL'  => '',
            'INDEX_ELEMENT'    => 'Y',
            'INDEX_SECTION'    => 'Y',
            'SECTIONS_NAME'    => 'Разделы',
            'SECTION_NAME'     => 'Раздел',
            'ELEMENTS_NAME'    => 'Элементы',
            'ELEMENT_NAME'     => 'Элемент',
            'GROUP_ID'         => [
                \core\Codex::GROUP_ID_ADMIN => \core\Codex::PERMISSION_FULL,
                \core\Codex::GROUP_ID_ALL   => \core\Codex::PERMISSION_READ,
            ],
            'FIELDS'           => [
                'IBLOCK_SECTION'  => [
                    'IS_REQUIRED' => 'N',
                ],
                'CODE'            => [
                    'IS_REQUIRED'   => 'N',
                    'DEFAULT_VALUE' => [
                        'UNIQUE'          => 'Y',
                        'TRANSLITERATION' => 'Y',
                        'TRANS_LEN'       => 100,
                        'TRANS_CASE'      => 'L',
                        'TRANS_SPACE'     => '-',
                        'TRANS_OTHER'     => '-',
                        'TRANS_EAT'       => 'Y',
                    ],
                ],
                'SECTION_CODE'    => [
                    'IS_REQUIRED'   => 'N',
                    'DEFAULT_VALUE' => [
                        'UNIQUE'          => 'Y',
                        'TRANSLITERATION' => 'Y',
                        'TRANS_LEN'       => 100,
                        'TRANS_CASE'      => 'L',
                        'TRANS_SPACE'     => '-',
                        'TRANS_OTHER'     => '-',
                        'TRANS_EAT'       => 'Y',
                    ],
                ],
                'PREVIEW_PICTURE' => [
                    'DEFAULT_VALUE' => [
                        'FROM_DETAIL' => 'Y',
                    ],
                ],
                'SECTION_PICTURE' => [
                    'DEFAULT_VALUE' => [
                        'FROM_DETAIL' => 'Y',
                    ],
                ],
            ],
        ]);

        if (empty($blockID)) {
            $error = trim(filter_var($block->LAST_ERROR, FILTER_SANITIZE_STRING));
            \yii\helpers\Console::printError($error);

            return false;
        }
    }

    public function safeDown()
    {
        CIBlock::Delete(Yii::$block->id__<?= $code; ?>__from__<?= $type; ?>);
    }
}
