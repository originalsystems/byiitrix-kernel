<?php

/**
 * @var \yii\base\View $this
 * @var string         $type
 */

echo "<?php\n";

if( !empty($namespace) ) {
    echo PHP_EOL . "namespace {$namespace};" . PHP_EOL;
}
?>

use yii\db\Migration;
use core\Codex;

class <?= $className; ?> extends Migration
{
    public function safeUp()
    {
        $type   = new \CIBlockType();
        $typeID = $type->Add([
            'ID'       => Codex::TYPE_<?= $type; ?>,
            'SECTIONS' => 'Y',
            'SORT'     => 100,
            'LANG'     => [
                'ru' => [
                    'NAME' => '<?= \yii\helpers\Inflector::humanize(mb_strtolower($type)); ?>',
                ],
                'en' => [
                    'NAME' => '<?= \yii\helpers\Inflector::humanize(mb_strtolower($type)); ?>',
                ],
            ],
        ]);

        if (empty($typeID)) {
            $error = trim(filter_var($type->LAST_ERROR, FILTER_SANITIZE_STRING));
            \yii\helpers\Console::printError($error);

            return false;
        }
    }

    public function safeDown()
    {
        \CIBlockType::Delete(Codex::TYPE_<?= $type; ?>);
    }
}
