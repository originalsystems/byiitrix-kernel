<?php

namespace byiitrix\components;

class PropertyEnum extends Base
{
    /**
     * Try parse name by regular expression and return whatever you want, like that:
     *
     * - id__Y__of__HAS_AIRPORT__in__city__from__content
     * returns integer id of PROPERTY_ENUM "Y" of PROPERTY_CODE "HAS_AIRPORT", IBLOCK "city", IBLOCK_TYPE "content"
     *
     * - get__Y__of__HAS_AIRPORT__in__city__from__content
     * returns array PROPERTY_ENUM "Y" of PROPERTY_CODE "HAS_AIRPORT", IBLOCK "city", IBLOCK_TYPE "content"
     *
     * - list_of__HAS_AIRPORT__in__city__from__content
     * returns list of property enums from PROPERTY_CODE "HAS_AIRPORT", IBLOCK "city", IBLOCK_TYPE "content"
     *
     * @param string $name
     *
     * @return array|int|null|string
     * @throws \Exception
     */
    public function __get($name)
    {
        if (preg_match('#^id__(?P<xml_id>.+?)__of__(?P<prop>.+?)__in__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $xml_id
             * @var string $prop
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->id($xml_id, $prop, $block, $type);
        }

        if (preg_match('#^get__(?P<xml_id>.+?)__of__(?P<prop>.+?)__in__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $xml_id
             * @var string $prop
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->get($xml_id, $prop, $block, $type);
        }

        if (preg_match('#^list_of__(?P<prop>.+?)__in__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $prop
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->listOf($prop, $block, $type);
        }

        return null;
    }

    /**
     * Empty setter, nothing to do
     *
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value)
    {
    }

    /**
     * @param string $xmlID
     * @param string $property
     * @param string $block
     * @param string $type
     *
     * @return int|null
     */
    public function id($xmlID, $property, $block, $type)
    {
        $key = "{$xmlID}:{$property}:{$block}:{$type}";

        return $this->getOrSet($key, function () use ($xmlID, $property, $block, $type) {
            /**
             * @var \CDatabase $DB
             */
            global $DB;

            $xmlID    = $DB->ForSql($xmlID);
            $property = $DB->ForSql($property);
            $block    = $DB->ForSql($block);
            $type     = $DB->ForSql($type);

            $sql = <<<SQL
SELECT 
    `b_iblock_property_enum`.`ID` AS `ID`

FROM `b_iblock_property_enum`

INNER JOIN `b_iblock_property` 
    ON `b_iblock_property`.`ID` = `b_iblock_property_enum`.`PROPERTY_ID`

INNER JOIN `b_iblock` 
    ON `b_iblock`.`ID` = `b_iblock_property`.`IBLOCK_ID`

WHERE `b_iblock_property_enum`.`XML_ID` = '{$xmlID}'
    AND `b_iblock_property`.`CODE` = '{$property}'
    AND `b_iblock`.`CODE` = '{$block}'
    AND `b_iblock`.`IBLOCK_TYPE_ID` = '{$type}'

LIMIT 1;
SQL;

            $row = $DB->Query($sql)->Fetch();

            return isset($row['ID']) ? (int)$row['ID'] : null;
        }, 3600);
    }

    /**
     * @param string $xmlID
     * @param string $property
     * @param string $block
     * @param string $type
     *
     * @return array|null
     */
    public function get($xmlID, $property, $block, $type)
    {
        $id = $this->id($xmlID, $property, $block, $type);

        if ($id === null) {
            return null;
        }

        return \CIBlockPropertyEnum::GetByID($id) ? : null;
    }

    /**
     * @param string $property
     * @param string $block
     * @param string $type
     *
     * @return array|bool|mixed
     * @throws \Exception
     */
    public function listOf($property, $block, $type)
    {
        $propertyID = \byiitrix\Core::$property->id($property, $block, $type);

        if (empty($propertyID)) {
            return array();
        }

        $rows   = array();
        $result = \CIBlockPropertyEnum::GetList(array('SORT' => 'ASC'), array('PROPERTY_ID' => $propertyID));

        while ($row = $result->GetNext()) {
            $rows[$row['ID']] = $row;
        }

        return $rows;
    }
}
