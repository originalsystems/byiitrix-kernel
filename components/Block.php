<?php

namespace byiitrix\components;

class Block extends Base
{
    /**
     * Try parse name by regular expression and return whatever you want, like that:
     *
     * - id__city__from__content
     * returns integer id IBLOCK "city",IBLOCK_TYPE "content"
     *
     *  - get__city__from__content
     * returns array IBLOCK "city", IBLOCK_TYPE "content"
     *
     * @param string $name
     *
     * @return array|int|null
     * @throws \Exception
     */
    public function __get($name)
    {
        if (preg_match('#^id__(?P<code>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $code
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->id($code, $type);
        }

        if (preg_match('#^get__(?P<code>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $code
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->get($code, $type);
        }

        return null;
    }

    /**
     * Empty setter, nothing to do
     *
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value)
    {
    }

    /**
     * @param string $code
     * @param string $type
     *
     * @return int|null
     */
    public function id($code, $type)
    {
        $key = "{$code}:{$type}";

        return $this->getOrSet($key, function () use ($code, $type) {
            /**
             * @var \CDatabase $DB
             */
            global $DB;

            $code = $DB->ForSql($code);
            $type = $DB->ForSql($type);

            $sql = <<<SQL
SELECT 
    `b_iblock`.`ID` AS `ID`

FROM `b_iblock`

WHERE `b_iblock`.`CODE` = '{$code}' 
    AND `b_iblock`.`IBLOCK_TYPE_ID` = '{$type}'

LIMIT 1;
SQL;

            $row = $DB->Query($sql)->Fetch();

            return isset($row['ID']) ? (int)$row['ID'] : null;
        }, 3600);
    }

    /**
     * @param string $code
     * @param string $type
     *
     * @return array|null
     * @throws \Exception
     */
    public function get($code, $type)
    {
        $id = $this->id($code, $type);

        if ($id === null) {
            return null;
        }

        return \CIBlock::GetByID($id)->GetNext() ? : null;
    }
}
