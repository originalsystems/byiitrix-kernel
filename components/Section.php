<?php

namespace byiitrix\components;

class Section extends Base
{
    /**
     * Try parse name by regular expression and return whatever you want, like that:
     *
     * - id__asia__in__city__from__content
     * returns integer id of SECTION "asia" from IBLOCK "city", IBLOCK_TYPE "content"
     *
     * - get__asia__in__city__from__content
     * returns array SECTION "asia" from IBLOCK "city", IBLOCK_TYPE "content"
     *
     * - list_of__city__from__content
     * returns list of section from IBLOCK "city", IBLOCK_TYPE "content"
     *
     * - root_list_of__city__from__content
     * returns root sections from IBLOCK "city", IBLOCK_TYPE "content"
     *
     * - children_of__asia__in__city__from__content
     * returns children sections of SECTION "asia" from IBLOCK "city", IBLOCK_TYPE "content"
     *
     * @param string $name
     *
     * @return array|int|null
     * @throws \Exception
     */
    public function __get($name)
    {
        if (preg_match('#^id__(?P<section>.+?)__in__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $section
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->id($section, $block, $type);
        }

        if (preg_match('#^get__(?P<section>.+?)__in__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $section
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->get($section, $block, $type);
        }

        if (preg_match('#^list_of__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->listOf($block, $type);
        }

        if (preg_match('#^root_list_of__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->listOf($block, $type, false);
        }

        if (preg_match('#^children_of__(?P<parent>[^_].+?[^_])__in__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $parent
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            $parent   = str_replace('___', '-', $parent);
            $parentID = $this->id($parent, $block, $type);

            if (empty($parentID)) {
                return array();
            }

            return $this->listOf($block, $type, $parentID);
        }

        return null;
    }

    /**
     * Empty setter, nothing to do
     *
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value)
    {
    }

    /**
     * @param string $section
     * @param string $block
     * @param string $type
     *
     * @return int
     */
    public function id($section, $block, $type)
    {
        $key = "{$section}:{$block}:{$type}";

        return $this->getOrSet($key, function () use ($section, $block, $type) {
            /**
             * @var \CDatabase $DB
             */
            global $DB;

            $section = $DB->ForSql($section);
            $block   = $DB->ForSql($block);
            $type    = $DB->ForSql($type);

            $sql = <<<SQL
SELECT 
    `b_iblock_section`.`ID` AS `ID`

FROM `b_iblock_section`

INNER JOIN `b_iblock` 
    ON `b_iblock`.`ID` = `b_iblock_section`.`IBLOCK_ID`

WHERE `b_iblock_section`.`CODE` = '{$section}'
    AND `b_iblock`.`CODE` = '{$block}'
    AND `b_iblock`.`IBLOCK_TYPE_ID` = '{$type}';
SQL;

            $row = $DB->Query($sql)->Fetch();

            return isset($row['ID']) ? (int)$row['ID'] : null;
        }, 3600);
    }

    /**
     * @param string $section
     * @param string $block
     * @param string $type
     *
     * @return array
     */
    public function get($section, $block, $type)
    {
        $id = $this->id($section, $block, $type);

        if ($id === null) {
            return null;
        }

        return \CIBlockSection::GetList(array(), array('ID' => $id))->GetNext() ? : null;
    }

    /**
     * Returns full list of section in infoblock
     *
     * @param string $block
     * @param string $type
     * @param string $parentID
     *
     * @return array
     */
    public function listOf($block, $type, $parentID = null)
    {
        $blockID = \byiitrix\Core::$block->id($block, $type);

        if (empty($blockID)) {
            return array();
        }

        $rows   = array();
        $filter = array(
            'IBLOCK_ID'     => $blockID,
            'ACTIVE'        => 'Y',
            'GLOBAL_ACTIVE' => 'Y',
        );

        if ($parentID !== null) {
            $filter['SECTION_ID'] = $parentID;
        }

        $result = \CIBlockSection::GetList(array('SORT' => 'ASC'), $filter);

        while ($row = $result->GetNext()) {
            $rows[$row['ID']] = $row;
        }

        return $rows;
    }
}
