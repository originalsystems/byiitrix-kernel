<?php

namespace byiitrix\components;

class Property extends Base
{
    /**
     * Try parse name by regular expression and return whatever you want, like that:
     *
     * - id__ZIP__in__city__from__content
     * returns integer id of PROPERTY "ZIP" from IBLOCK "city", IBLOCK_TYPE "content"
     *
     * - get__ZIP__in__city__from__content
     * returns array PROPERTY "ZIP" from IBLOCK "city", IBLOCK_TYPE "content"
     *
     * - list_of__city__from__content
     * returns list of properties from IBLOCK "city", IBLOCK_TYPE "content"
     *
     * @param string $name
     *
     * @return array|int|null
     * @throws \Exception
     */
    public function __get($name)
    {
        if (preg_match('#^id__(?P<prop>.+?)__in__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $prop
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->id($prop, $block, $type);
        }

        if (preg_match('#^get__(?P<prop>.+?)__in__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $prop
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->get($prop, $block, $type);
        }

        if (preg_match('#^list_of__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->listOf($block, $type);
        }

        return null;
    }

    /**
     * Empty setter, nothing to do
     *
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value)
    {
    }

    /**
     * @param string $property
     * @param string $block
     * @param string $type
     *
     * @return int|null
     */
    public function id($property, $block, $type)
    {
        $key = "{$property}:{$block}:{$type}";

        return $this->getOrSet($key, function () use ($property, $block, $type) {
            /**
             * @var \CDatabase $DB
             */
            global $DB;

            $property = $DB->ForSql($property);
            $block    = $DB->ForSql($block);
            $type     = $DB->ForSql($type);

            $sql = <<<SQL
SELECT 
    `b_iblock_property`.`ID` AS `ID`

FROM `b_iblock_property`

INNER JOIN `b_iblock` 
    ON `b_iblock`.`ID` = `b_iblock_property`.`IBLOCK_ID`

WHERE `b_iblock_property`.`CODE` = '{$property}' 
    AND `b_iblock`.`CODE` = '{$block}' 
    AND `b_iblock`.`IBLOCK_TYPE_ID` = '{$type}'

LIMIT 1;
SQL;

            $row = $DB->Query($sql)->Fetch();

            return isset($row['ID']) ? (int)$row['ID'] : null;
        }, 3600);
    }

    /**
     * @param string $property
     * @param string $block
     * @param string $type
     *
     * @return array|null
     */
    public function get($property, $block, $type)
    {
        $id = $this->id($property, $block, $type);

        if ($id === null) {
            return null;
        }

        return \CIBlockProperty::GetByID($id)->GetNext() ? : null;
    }

    /**
     * @param string $block
     * @param string $type
     *
     * @return array
     */
    public function listOf($block, $type)
    {
        $blockID = \byiitrix\Core::$block->id($block, $type);

        if (empty($blockID)) {
            return array();
        }

        $rows   = array();
        $result = \CIBlockProperty::GetList(array('SORT' => 'ASC'), array(
            'IBLOCK_ID' => $blockID,
        ));

        while ($row = $result->GetNext()) {
            $rows[$row['ID']] = $row;
        }

        return $rows;
    }
}
