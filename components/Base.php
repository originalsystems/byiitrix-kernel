<?php

namespace byiitrix\components;

abstract class Base
{
    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * @var Base[]
     */
    private static $_instances = array();

    /**
     * @return static
     */
    public static function instance()
    {
        if (isset(self::$_instances[static::class]) === false) {
            self::$_instances[static::class] = new static();
        }

        return self::$_instances[static::class];
    }

    protected function getOrSet($key, callable $callable, $duration)
    {
        $dir   = mb_strtolower(str_replace('\\', '_', static::class));
        $cache = new \CPHPCache();

        if ($cache->InitCache($duration, $key, $dir)) {
            $vars = $cache->GetVars();

            return $vars['result'];
        }

        $result = $callable();

        if ($cache->StartDataCache()) {
            $cache->EndDataCache(array('result' => $result));
        }

        return $result;
    }
}
