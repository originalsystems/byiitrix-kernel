<?php

namespace byiitrix\components;

class Element extends Base
{
    /**
     * Try parse name by regular expression and return whatever you want, like that:
     *
     * - id__asia__in__city__from__content
     * returns integer id of SECTION "asia" from IBLOCK "city", IBLOCK_TYPE "content"
     *
     * - get__asia__in__city__from__content
     * returns array SECTION "asia" from IBLOCK "city", IBLOCK_TYPE "content"
     *
     * - list_of__city__from__content
     * returns list of section from IBLOCK "city", IBLOCK_TYPE "content"
     *
     * @param string $name
     *
     * @return array|int|null
     * @throws \Exception
     */
    public function __get($name)
    {
        if (preg_match('#^id__(?P<code>[^_].+?[^_])__in__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $code
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            $code = str_replace('___', '-', $code);

            return $this->id($code, $block, $type);
        }

        if (preg_match('#^get__(?P<code>[^_].+?[^_])__in__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $code
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            $code = str_replace('___', '-', $code);

            return $this->get($code, $block, $type);
        }

        if (preg_match('#^list_of__(?P<block>.+?)__from__(?P<type>.+?)$#', $name, $matches)) {
            /**
             * @var string $block
             * @var string $type
             */

            extract($matches, EXTR_OVERWRITE);

            return $this->listOf($block, $type);
        }

        return null;
    }

    /**
     * Empty setter, nothing to do
     *
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value)
    {
    }

    /**
     * @param string $element
     * @param string $code
     * @param string $type
     *
     * @return int|null
     * @throws \Exception
     */
    public function id($element, $code, $type)
    {
        $key = "{$element}:{$code}:{$type}";

        return $this->getOrSet($key, function () use ($element, $code, $type) {
            /**
             * @var \CDatabase $DB
             */
            global $DB;

            $element = $DB->ForSql($element);
            $code    = $DB->ForSql($code);
            $type    = $DB->ForSql($type);

            $sql = <<<SQL
SELECT 
    `b_iblock_element`.`ID` AS `ID`

FROM `b_iblock_element`

INNER JOIN `b_iblock` 
    ON `b_iblock`.`ID` = `b_iblock_element`.`IBLOCK_ID`

WHERE  
    `b_iblock_element`.`CODE` = '{$element}' 
    AND `b_iblock`.`CODE` = '{$code}' 
    AND `b_iblock`.`IBLOCK_TYPE_ID` = '{$type}'

LIMIT 1;
SQL;

            $row = $DB->Query($sql)->Fetch();

            return isset($row['ID']) ? (int)$row['ID'] : null;
        }, 3600);
    }

    /**
     * @param string $element
     * @param string $code
     * @param string $type
     *
     * @return array|null
     * @throws \Exception
     */
    public function get($element, $code, $type)
    {
        $id = $this->id($element, $code, $type);

        if ($id === null) {
            return null;
        }

        /**
         * @var \_CIBElement $record
         */

        $record = \CIBlockElement::GetList(array(), array('ID' => $id))->GetNextElement() ? : null;

        if ($record === null) {
            return null;
        }

        $fields               = $record->fields;
        $fields['PROPERTIES'] = $record->GetProperties();

        return $fields;
    }

    /**
     * @param string $code
     * @param string $type
     *
     * @return array
     */
    public function listOf($code, $type)
    {
        $blockID = \byiitrix\Core::$block->id($code, $type);

        if ($blockID === null) {
            return array();
        }

        $rows   = array();
        $result = \CIBlockElement::GetList(array('SORT' => 'ASC'), array(
            'IBLOCK_ID' => $blockID,
            'ACTIVE'    => 'Y',
        ));

        while ($element = $result->GetNextElement()) {
            $fields               = $element->fields;
            $fields['PROPERTIES'] = $element->GetProperties();

            $rows[$fields['ID']] = $fields;
        }

        return $rows;
    }
}
