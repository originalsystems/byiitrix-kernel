<?php

namespace byiitrix\log;

use yii\helpers\VarDumper;
use yii\log\FileTarget;

class PlainFileTarget extends FileTarget
{
    public function formatMessage($message)
    {
        list($text, $level, $category, $timestamp) = $message;

        if (is_string($text) === false) {
            // exceptions may not be serializable if in the call stack somewhere is a Closure
            if ($text instanceof \Throwable || $text instanceof \Exception) {
                $text = (string)$text;
            } else {
                $text = VarDumper::export($text);
            }
        }

        return date('[Y-m-d H:i:s]: ', $timestamp) . $text;
    }
}
