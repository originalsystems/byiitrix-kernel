<?php

namespace byiitrix;

class Core
{
    /**
     * @var \byiitrix\components\Block|\common\components\Block
     */
    public static $block;

    /**
     * @var \byiitrix\components\Element|\common\components\Element
     */
    public static $element;

    /**
     * @var \byiitrix\components\Property|\common\components\Property
     */
    public static $property;

    /**
     * @var \byiitrix\components\PropertyEnum|\common\components\PropertyEnum
     */
    public static $propertyEnum;

    /**
     * @var \byiitrix\components\Section|\common\components\Section
     */
    public static $section;
}

Core::$block        = \byiitrix\components\Block::instance();
Core::$element      = \byiitrix\components\Element::instance();
Core::$property     = \byiitrix\components\Property::instance();
Core::$propertyEnum = \byiitrix\components\PropertyEnum::instance();
Core::$section      = \byiitrix\components\Section::instance();
